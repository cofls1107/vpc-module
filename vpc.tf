# validation
resource "null_resource" "validate_account" {
  count = var.current_id == var.account_id ? 0 : "Please check that you are using the AWS account"
}

resource "null_resource" "validate_module_name" {
  count = local.module_name == var.tags["TerraformModuleName"] ? 0 : "Please check that you are using the Terraform module"
}

resource "null_resource" "validate_module_version" {
  count = local.module_version == var.tags["TerraformModuleVersion"] ? 0 : "Please check that you are using the Terraform module"
}

# vpc
resource "aws_vpc" "this" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support   = true
  instance_tenancy     = "default"

  tags = merge(var.tags, tomap({"Name" = format("%s-%s-vpc", var.prefix, var.vpc_name)}))
}

# dynamic subnet
resource "aws_subnet" "this" {
  vpc_id            = aws_vpc.this.id
  for_each          = { for i in local.all_subnets : i.cidr => i }
  cidr_block        = each.key
  availability_zone = var.azs[index(var.subnets[each.value.name].cidr, each.key)]

  tags = merge(var.tags, 
    tomap({
      Name = format(
        "%s-%s-%s-subnet-%s", 
        var.prefix,
        var.vpc_name,
        each.value.name,
        substr(var.azs[index(var.subnets[each.value.name].cidr, each.key)], -2, -1)
      )
    }),
    var.subnets[each.value.name].ipv4_type == "private" ? { "Tier" = "private" } : {},
    var.subnets[each.value.name].ipv4_type == "public"  ? { "Tier" = "public"  } : {}
  )
}


# internet gateway
resource "aws_internet_gateway" "this" {
  vpc_id = var.enable_internet_gateway ? aws_vpc.this.id : ""
  tags   = merge(var.tags, tomap({Name = format("%s-%s-igw", var.prefix, var.vpc_name)}))
}

# eip for nat gateway
resource "aws_eip" "nat" {
  vpc   = true
  count = "${ var.enable_internet_gateway && var.enable_nat_gateway ? length(var.azs) : 0 }"

  tags = merge(var.tags,
    tomap({
      Name = format(
        "%s-%s-eip-%s",
        var.prefix,
        var.vpc_name,
        substr(var.azs[count.index], -2, -1)
      )
    }))
}

# nat gateway
resource "aws_nat_gateway" "this" {
  for_each      = { for k, v in local.natgw_subnets : k => v }
  allocation_id = aws_eip.nat[index(var.subnets[each.value.name].cidr, each.value.cidr)].id
  subnet_id     = aws_subnet.this[each.value.cidr].id
  
  depends_on = [
    aws_internet_gateway.this
  ]

  tags = merge(var.tags,
    tomap({
      Name = format(
        "%s-%s-%s-%s",
        var.prefix,
        var.vpc_name,
        each.value.name,
        substr(var.azs[index(var.subnets[each.value.name].cidr, each.value.cidr)], -2, -1)
      )
    })
  )
}