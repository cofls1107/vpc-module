locals {
  module_name    = "terraform-aws-module-vpc-v2"
  module_version = "v1.0.0"
}

locals {
  all_subnets = flatten([
    for key, value in var.subnets : [
      for item in value.cidr : {
        name = key
        cidr = item
      }
    ]
  ])
}

locals {
  natgw_subnets = flatten([
    for key, value in var.subnets : [
      for item in value.cidr : {
        name = key
        cidr = item
      }
    ] if value.ipv4_type == "public" && value.natgw_subnet == "yes"
  ])
}