output "account_id" {
  description = "AWS Account ID"
  value       = var.account_id
}

output "current_id" {
  description = "Your current AWS Account ID"
  value       = var.current_id
}

output "region" {
  description = "AWS region"
  value       = var.region
}

output "current_region" {
  description = "Your current AWS region"
  value       = var.current_region
}

output "vpc_id" {
  description = "VPC ID"
  value = aws_vpc.this.id
}

output "vpc_cidr_block" {
  description = "CIDR block for VPC"
  value = aws_vpc.this.cidr_block
}

output "subnet_ids" {
  description = "Subnet ID List"
  value = [aws_subnet.this]
}