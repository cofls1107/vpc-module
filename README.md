# AWS VPC Terraform custom module
* AWS 에서 VPC 리소스를 생성하는 공통 모듈
* subnet을 가변적으로 늘리고 줄일 수 있는 구조
* 입력한 Account ID와 current id(terraform를 구동하는 실제 aws account)가 다르면 예외(raise) 발생
* region과 current_region 에 대한 output 메시지 확인 필수

## Usage

### `terraform.tfvars`
* 모든 변수는 적절하게 변경하여 사용
```
account_id = "012345678912" # 아이디 변경 필수
region     = "ap-northeast-2" # 리전 변경 필수
prefix     = "dev"
vpc_name   = "common" # 최종 VPC 이름: ${prefix}-${vpc_name}-vpc
vpc_cidr   = "10.0.0.0/16" # 원하는 대역으로 변경(/16 ~ /28 가능)
azs        = ["ap-northeast-2a", "ap-northeast-2c"] # AZ 개수와 서브넷(cidr)은 1:1 매핑, 개수는 달라도 되지만 빈 값은 올 수 없음 

enable_internet_gateway = true
enable_nat_gateway      = true # 사용할려면 enable_internet_gateway = true 필수

subnets = {
  bastion = {
    cidr         = ["10.0.2.0/24", "10.0.3.0/24"],
    ipv4_type    = "public"
    natgw_subnet = "yes" # 사용할려면 enable_nat_gateway = true 필수
  },
  eks = {
    cidr         = ["10.0.4.0/24", "10.0.5.0/24"],
    ipv4_type    = "private"
    natgw_subnet = "no"
  },
  gitlab = {
    cidr         = ["10.0.6.0/24", "10.0.7.0/24"],
    ipv4_type    = "private"
    natgw_subnet = "no"
  },
  aurora = {
    cidr         = ["10.0.8.0/24", "10.0.9.0/24"],
    ipv4_type    = "private"
    natgw_subnet = "no"
  },
  elasticache = {
    cidr         = ["10.0.10.0/24", "10.0.11.0/24"],
    ipv4_type    = "private"
    natgw_subnet = "no"
  },
  memorydb = {
    cidr         = ["10.0.12.0/24", "10.0.13.0/24"],
    ipv4_type    = "private"
    natgw_subnet = "no"
  },
  ad = {
    cidr         = ["10.0.14.0/24", "10.0.15.0/24"],
    ipv4_type    = "private"
    natgw_subnet = "no"
  }
}

# 공통 tag, 생성되는 모든 리소스에 태깅
tags = {
  "CreatedByTerraform"     = "true"
  "TerraformModuleName"    = "terraform-aws-module-vpc-v2"
  "TerraformModuleVersion" = "v1.0.0"
}
```
---

### `main.tf`
```
module "vpc" {
  source = "git::https://mz.gitlab.kis.finance/mzc/terraform-aws-module-vpc-v2.git?ref=v1.0.0"

  account_id = var.account_id
  region     = var.region
  prefix     = var.prefix

  vpc_name = var.vpc_name
  vpc_cidr = var.vpc_cidr
  subnets  = var.subnets
  azs      = var.azs

  enable_internet_gateway = var.enable_internet_gateway
  enable_nat_gateway      = var.enable_nat_gateway
  
  current_id     = data.aws_caller_identity.current.account_id
  current_region = data.aws_region.current.name

  tags = var.tags
}
```
---

### `provider.tf`
```
provider "aws" {
  region = var.region
}
```
---

### `terraform.tf`
```
terraform {
  required_version = ">= 1.1.2"
  
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.70"
    }
  }

  # bucket과 key 생성 룰
  # kis-{mts, mps}-{dev, test, stage, prod}-tf-state-backend/${ACCOUNT_ID}/{dev, test, stage, prod}/${VPC_LINEUP}
  backend "s3" {
    bucket         = "kis-mts-dev-tf-state-backend"
    key            = "012345678912/dev/common/terraform.state"
    region         = "ap-northeast-2"
    dynamodb_table = "terraform-state-locks" # 다이나모 테이블 이름 변경 가능(필요 시)
    encrypt        = true
  }
}
```
---

### `data.tf`
```
data "aws_region" "current" {}

data "aws_caller_identity" "current" {}
```
---

### `variables.tf`
```
variable "region" {
  description = "AWS Region"
  type        = string
}

variable "account_id" {
  description = "Allowed AWS account IDs"
  type        = string
}

variable "prefix" {
  description = "prefix for aws resources and tags"
  type        = string
}

variable "vpc_name" {
  description = "VPC name"
  type        = string
}

variable "vpc_cidr" {
  description = "VPC default cidr"
  type        = string
}

variable "azs" {
  description = "Availability Zone List"
  type        = list
}

variable "tags" {
  description = "tag map"
  type        = map(string)
}

variable "subnets" {
  description = "subnet list"
  type        = map(any)
}

variable "enable_internet_gateway" {
  description = "internet gateway whether or not use"
  type        = bool
}

variable "enable_nat_gateway" {
  description = "nat gateway whether or not use"
  type        = bool
}
```
---

### `outputs.tf`
```
output "aws-vpc" {
  description = "AWS VPC"
  value = module.vpc
}
```

## 실행방법
```
terraform init -get=true -upgrade -reconfigure
terraform validate (option)
terraform plan -var-file=terraform.tfvars -refresh=false -out=planfile
terraform apply planfile
```
* "Objects have changed outside of Terraform" 때문에 `-refresh=false`를 사용
* 실제 UI에서 리소스 변경이 없어보이는 것과 low-level Terraform에서 Object 변경을 감지하는 것에 차이가 있는 것 같음, 다음 링크 참고
    * https://github.com/hashicorp/terraform/issues/28776
* 위 이슈로 변경을 감지하고 리소스를 삭제하는 케이스가 발생 할 수 있음